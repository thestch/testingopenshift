#!/bin/env groovy

def appName = 'user'
def releasedVersion = ''

environment {
 imagename = "user-imagestream"
 dockerImage = ''
}

pipeline {
    // Use the 'maven' Jenkins agent image which is provided with OpenShift
    agent { label "maven" }
    stages {
        stage("Checkout") {
            steps {
                checkout scm
            }
        }

		stage("Compile"){

			steps {

			    script {
                    releasedVersion = getReleaseVersion()
                    echo "${releasedVersion}"
                }

                sh """
                    mvn clean compile
                """
			}
		}

        stage("Unit Test"){
            steps {
                sh """
				mvn package
			  """
            }
        }

        stage("Sonarqube analysis"){
            steps{
                script{
                    echo "Sonarqube analysis"
                }
            }
        }

        stage("Deploy artifacts"){
            steps{
                script{
                    echo "Deploy maven artifacts"
                }
            }
        }

        stage("Docker build"){
            steps{
                sh """
                    
                    oc project libertykafka
                    oc delete is ${appName}-imagestream || true
                    oc delete bc ${appName}-buildconfig || true
                    oc delete all --selector app=${appName} || true
                    oc delete OpenLibertyApplication ${appName} || true

                    oc process -f build.yaml -p APP_NAME=${appName} -p VERSION=${releasedVersion} | oc create -f -
                    oc start-build ${appName}-buildconfig --from-dir=. --follow

                    sleep 10

                    oc apply -f deploy.yaml
                    
                """
            }
        }
    }
}

def getReleaseVersion() {
    def pom = readMavenPom file: 'pom.xml'
    def gitCommit = sh(returnStdout: true, script: 'git rev-parse --short HEAD').trim()
    def versionNumber
    if (gitCommit == null) {
        versionNumber = env.BUILD_NUMBER
    } else {
        versionNumber = env.BUILD_NUMBER + gitCommit
    }
    return pom.version.replace("-SNAPSHOT", ".${versionNumber}")
}

//oc tag user-imagestream:${releasedVersion} user-imagestream:latest