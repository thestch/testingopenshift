package com.example.demo.domain;


import javax.persistence.*;

@Entity
@Table(name="USER")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private final String name;
    private final String email;
    private final String departmentid;

    public User() {
        this.name = "";
        this.email = "";
        this.departmentid = "";
    }

    public User(String name, String email, String departmentid) {
        this.name = name;
        this.email = email;
        this.departmentid = departmentid;
    }
//
//    public User(String name, String email, int departmentid) {
//        this.name = name;
//        this.email = email;
//        this.departmentid = departmentid;
//    }
//
//    public User(String name, String email, String departmentid) {
//        this.name = name;
//        this.email = email;
//        this.departmentid = Long.valueOf(departmentid);
//    }


    public long getId() {
        return id;
    }
    public void setId(long id) {
        this.id = id;
    }

    public String getDepartmentid() {
        return departmentid;
    }



    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    @Override
    public String toString() {
        return "User{" + "id=" + id + ", name=" + name + ", email=" + email
                + ", departmentid=" + departmentid + '}';
    }
}