package com.example.demo.kafka.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.kafka.common.serialization.Serializer;

import java.util.Objects;

public class EventSerializer implements Serializer<Object> {

    @Override
    public byte[] serialize(String topic, Object data){
        byte[] serializedValue = null;

        ObjectMapper objectMapper = new ObjectMapper();
        if(Objects.nonNull(data)){
            try{
                serializedValue = objectMapper.writeValueAsBytes(data);
            }catch(JsonProcessingException jpe){

            }
        }

        return serializedValue;
    }
}
