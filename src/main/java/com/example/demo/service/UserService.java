package com.example.demo.service;

import com.example.demo.domain.User;
import com.example.demo.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;
import java.util.List;


@Service
public class UserService implements UserServiceInterface {

    Logger LOGGER = LoggerFactory.getLogger(getClass());

    @Autowired
    RestTemplate template;

    private final UserRepository userRepository;

    public UserService(UserRepository userRepository){
        this.userRepository = userRepository;
    }

    @PostConstruct
    public void init(){
        User user1 = new User("John", "john@domain.com", "1");

        User user2 = new User("Julie", "julie@domain.com", "1");

        User user3 = new User("Jennifer", "jennifer@domain.com", "1");

        User user4 = new User("Helen", "helen@domain.com", "2");

        User user5 = new User("Rachel", "rachel@domain.com", "2");

        User user6 = new User("Sam", "sam@gmail.com", "2");

        userRepository.save(user1);
        userRepository.save(user2);
        userRepository.save(user3);
        userRepository.save(user4);
        userRepository.save(user5);
        userRepository.save(user6);

    }

    @Override
    public User findUserById(Long theId) {

        return userRepository.findById(theId).orElse(null);
    }

    @Override
    public void saveUser(User user) {
        userRepository.save(user);
    }

    @Override
    public List<User> findAll() {
        return (List<User>) userRepository.findAll();
    }
}