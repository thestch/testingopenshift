package com.example.demo.service;

import com.example.demo.domain.User;

import java.util.List;

public interface UserServiceInterface {



    public User findUserById(Long theId);

    public void saveUser(User theTransaction);

    public List<User> findAll();

}