package com.example.demo.controller;

import com.example.demo.VO.Department;
import com.example.demo.VO.ResponseTemplateVO;
import com.example.demo.domain.User;
import com.example.demo.kafka.dto.Greeting;
import com.example.demo.kafka.producer.EventProducer;
import com.example.demo.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.List;

//@CrossOrigin(origins = "http://frontendopenshift-libertykafka.apps.vrls2.60af.sandbox23.opentlc.com")
//@CrossOrigin(origins="http://localhost:4200")
@RestController
public class UserController {
    Logger logger = LoggerFactory.getLogger(getClass());

    // standard constructors
    private final UserService userService;

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private EventProducer producer;

    public UserController(UserService userService) {
        this.userService = userService;
    }


    @GetMapping("/users")
    public List<User> getUsers() {
        return (List<User>) userService.findAll();
    }

    @PostMapping("/users")
    void addUser(@RequestBody User user) {

        logger.info("asddd" + user.toString());
        userService.saveUser(user);

    }

    @GetMapping("/users/{id}")
    public ResponseTemplateVO getUserWithDepartment(@PathVariable("id") Long id) {
        User user = userService.findUserById(id);
        Department department = restTemplate.getForObject("http://department-libertykafka.apps.vehsystempoc.ade4.sandbox116.opentlc.com/department/" + user.getDepartmentid(), Department.class);
        ResponseTemplateVO vo = new ResponseTemplateVO(user ,department);
        return vo;
    }

    @GetMapping("/greet/{name}/{message}")
    public void send(@PathVariable("name") String name, @PathVariable("message") String message){
        Greeting greeting = new Greeting();
        greeting.setName(name);
        greeting.setMessage(message);

        producer.send("my-topic", "1", greeting);
    }
}